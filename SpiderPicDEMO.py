import requests
from requests.exceptions import RequestException
import re
import json


def get_page(url, header):
    try:
        response = requests.get(url, headers=header)
        if response.status_code == 200:
            return response.text
        else:
            return None
    except RequestException:
        print(RequestException)
        return None


def parse_page(html):
    pattern = re.compile('gallery.*?<noscript><img src="(.*?)".*?</noscript>.*?<div class="caption">(.*?)</div>', re.S)
    items = re.findall(pattern, html)
    print(items)
    for item in items:
        yield {
            'name': item[1],
            'picPath': item[0]
        }


def write(content):
    with open('D:/IdeaWorkSpace/spider/result.txt', 'a', encoding='UTF-8') as f:
        f.write(json.dumps(content, ensure_ascii=False) + '\n')
        f.close()


def main():
    url = 'https://<urllink i want>'
    header = {
        'User_Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0'
    }
    for i in range(11):
        urlcurent = url + str(i) + '/'
        print(urlcurent)
        html = get_page(urlcurent, header)

        #print(html)

        for item in parse_page(html):
            write(item)


if __name__ == '__main__':
    main()





