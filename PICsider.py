import requests
from requests.exceptions import RequestException
import re
import os
from hashlib import md5



def get_page(url, header):
    try:
        response = requests.get(url, headers=header)
        if response.status_code == 200:
            return response.text
        else:
            return None
    except RequestException:
        print(RequestException)
        return None


#/s/23141/
def parse_list_ReturnDetailPath(html):
    pattern = re.compile('<a href="(.*?)" class="cover"')
    items = re.findall(pattern, html)
    for item in items:
        yield 'https://zh.d-upp.net' + str(item)



#/g/12341/#1--n
def parse_detail_ReturnPicURL(html):
    pattern = re.compile('<a href="(.*?)" class="cover"')
    items2 = re.findall(pattern, html)
    str = ('https://zh.d-upp.net' + items2[0])[:-2]

    return str


def parse_pic_RetrunDownloadURL(html):
    pattern = re.compile('class="img-url">//j(.*?)</div>')
    items1 = re.findall(pattern, html)
    for item in items1:
        yield 'https://c.mipcdn.com/i/s/https://' + str(item)



def downloadPIC(url, header,num):

    try:
        response = requests.get(url, headers=header)
        if response.status_code == 200:
            # file = '{0}/{1}.{2}'.format(os.getcwd() + '\\pic', md5(response.content).hexdigest(), 'jpg')
            file = '{0}/{1}.{2}'.format(os.getcwd() + '\\pic', str(num), 'jpg')
            with open(file, 'wb') as f:
                f.write(response.content)
                f.close()
        else:
            return None
    except RequestException:
        print(RequestException)
        return None


def main():
    url = 'https://zh.d-upp.net/page/'
    pages = 1
    header = {
        'User_Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0'
    }
    for i in range(pages):
        listHTML = get_page(url + str(i+1) + '/', header)
        for detailpath in parse_list_ReturnDetailPath(listHTML):
            picHTML = get_page(detailpath, header)
            viewHTML = get_page(parse_detail_ReturnPicURL(picHTML), header)
            for downLoad in parse_pic_RetrunDownloadURL(viewHTML):
                downloadPIC(downLoad, header)


    viewHTML = get_page('<urllink you want>', header)
    num = 0
    for downLoad in parse_pic_RetrunDownloadURL(viewHTML):
        downloadPIC(downLoad, header, num)
        num+=1

if __name__ == '__main__':
    main()

